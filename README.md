# vue-gs

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


>环境变量：
>env.dev     开发环境
>env.prod    正式环境
>env.test    测试环境
>router      路由管理
>components  页面组件封装
>api         基础工具类方法封装
>api request.js  对axios进行二次封装，错误统一处理，数据请求单独管理