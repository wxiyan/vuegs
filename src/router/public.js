// 公共路由

const _import = require("./_import_" + process.env.NODE_ENV);

export default [
  {
    path: "/Login",
    name: "login",
    meta: { title: "登录" },
    component: _import("Community/Login"),
  },
  {
    path: "/PrivacyPolicy",
    name: "PrivacyPolicy",
    meta: { title: "隐私政策" },
    component: _import("Public/PrivacyPolicy"),
  },
  {
    path: "/SaleContract",
    name: "SaleContract",
    meta: { title: "《感算商城商品（服务）买卖合同》" },
    component: _import("Public/SaleContract"),
  },
  {
    path: "/ForumAgreement",
    name: "ForumAgreement",
    meta: { title: "《感知社区协议》" },
    component: _import("Public/ForumAgreement"),
  },
  {
    path: "/SiteMap",
    name: "SiteMap",
    meta: { title: "网站地图" },
    component: _import("Public/SiteMap"),
  },
  {
    path: "/404",
    name: "404",
    meta: { title: "404" },
    component: _import("Public/404"),
  },
  {
    path: "*",
    redirect: "/404",
  },
];
