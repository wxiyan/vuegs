// 跟用户信息相关的请求
import Store from "@/store/index.js";

// 登录
export function login({ UserMobile, PassWord }) {
  return request({
    url: "/api/index.php?m=user&c=Users&a=login",
    method: "post",
    data: `LoginState=login&UserMobile=${UserMobile}&PassWord=${PassWord}&ClientType=sH5`,
  });
}

// 获取用户信息
export function getUserInfo() {
  return request({
    url: "/api/index.php?m=user&c=Users&a=login",
    method: "post",
    data: `LoginState=getUser&ClientType=sH5&userToken=${Store.getters.token}`,
  });
}
