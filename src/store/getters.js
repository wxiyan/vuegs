// vuex 计算属性
export default {
  token: (state) => state.token,
  userCode: (state) => state.user.userCode,
  userName: (state) => state.user.userName,
  userImage: (state) => state.user.userImage,
  userMobile: (state) => state.user.userMobile,
  userType: (state) => state.user.userType,
  appSource: (state) => state.app.appSource,
  commentRefresh: (state) => state.comment.commentRefresh,
  targetUser: (state) => state.targetUser,
};
