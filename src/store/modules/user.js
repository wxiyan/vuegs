import { getToken, setToken, removeToken } from "@/utils/auth.js";
import { getUserInfo } from "@/api/user.js";
import Router from "@/router/index.js";

const user = {
  state: {
    token: getToken(),
    userCode: "", // 用户编号
    userName: "", // 用户名称
    userImage: "", // 用户图片
    userMobile: "", // 用户手机号
    userType: -1, // 角色类型
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
      setToken(token);
    },
    // 设置用户信息
    SET_USER_INFO: (state, userInfo) => {
      state.userCode = userInfo.userCode;
      state.userName = userInfo.userName;
      state.userImage = userInfo.headImg;
      state.userMobile = userInfo.mobile;
      state.userType = userInfo.userType;
    },
  },
  actions: {
    // 获取用户信息
    async GetUserInfo({ commit, dispatch }) {
      await new Promise((resolve) => {
        getUserInfo().then((res) => {
          if (res.success) {
            commit("SET_USER_INFO", res);
          } else {
            dispatch("FedLogOut");
          }
          resolve();
        });
      });
    },
    // 前端登出
    FedLogOut({ commit }) {
      // 清空数据
      commit("SET_TOKEN", "");
      commit("SET_USER_INFO", {});
      removeToken();
      // 跳转到登录页
      const url = toPublicLogin();
      if (url) {
        window.location.href = url;
      } else {
        Router.push("/login");
      }
    },
  },
};

export default user;
